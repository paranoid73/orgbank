![logo projet](https://i.imgur.com/bIggJwa.png)



#### ORG BANK**

> projet universitaire 2018-2019 dont l'objet est de créer un logiciel de micro finance en c++ avec une bibilothèque graphique de notre choix .

##### SCREENSHOTS

###### Formulaire de connexion

![connexion](https://i.imgur.com/QKZDgJF.png)

client

![](https://i.imgur.com/2IfgjrA.png)

###### Partie operation bancaire

![](https://i.imgur.com/YTfMlH3.png)



##### FRAMEWORKS

![logo qt](<https://ddgobkiprc33d.cloudfront.net/bb9f67d1-3dc4-46a8-a767-fc0da3035553.png>)

##### **SGBD**

![](https://www.sqlite.org/images/sqlite370_banner.gif)

