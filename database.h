#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>

class Database
{
public:
    static QSqlDatabase mydb;

    Database();
    static bool getConnexion();
};

#endif // DATABASE_H
