#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMouseEvent>
#include <QMessageBox>
#include "secondform.h"
#include "database.h"
#include "session.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
  m_Diff = event->pos();

  setCursor(QCursor(Qt::SizeAllCursor));
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
  Q_UNUSED(event);

  setCursor(QCursor(Qt::ArrowCursor));
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
  QPoint p = event->globalPos();

  window()->move(p - m_Diff);
}

void MainWindow::on_btnLogin_clicked()
{
    Database db;

    if(ui->inputName->text().isEmpty() || ui->inputPass->text().isEmpty()){
        QMessageBox msgBox;
        msgBox.setText("Veuillez remplir tous les champs .");
        msgBox.exec();
    }
    else if(db.getConnexion())
    {
        QString name = ui->inputName->text();
        QString pass = ui->inputPass->text();

        QSqlQuery query;
        query.exec("SELECT * FROM personne WHERE login = '"+name+"' AND motdepasse ='"+pass+"'");

        int compteur =0;
        while(query.next()){
            compteur++;
           session_id =  query.value(0).toInt();
           session_nom =  query.value(4).toString()+" "+query.value(5).toString();
           session_pass = pass;
        }

        if(compteur >= 1){
            this->hide();
            secondForm *sf = new secondForm();
            sf->show();
        }else{
            QMessageBox msgBox;
            msgBox.setText("Identifiant de connexion ou mot de passe incorrecte.");
            msgBox.exec();
        }
    /* sinon */
    }else{
        QMessageBox msgBox;
        msgBox.setText("Erreur de connexion à la base de donnée.");
        msgBox.exec();
    }

}

void MainWindow::on_btnExit_clicked()
{
    this->close();
}
