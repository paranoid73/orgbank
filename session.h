#ifndef SESSION_H
#define SESSION_H
#include <QtGlobal>


QT_BEGIN_NAMESPACE
class QString;
QT_END_NAMESPACE

/*  utilisateur */
extern qint32 session_id;
extern QString session_nom;
extern QString session_pass;
extern QString session_compte;
extern qint32 session_solde;
#endif // SESSION_H
