#ifndef GENERATOR_H
#define GENERATOR_H
#include <QString>

class Generator
{
public:
    Generator();
    static QString iban();
    static QString compteBancaire();
    static QString identifiant(QString nom);
};

#endif // GENERATOR_H
