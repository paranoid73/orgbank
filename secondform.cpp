#include "secondform.h"
#include "ui_secondform.h"
#include <QMouseEvent>
#include <database.h>
#include "session.h"
#include <QMessageBox>
#include <QProcess>
#include <QDebug>
#include <QSqlError>
#include <QDate>
#include "generator.h"

secondForm::secondForm(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::secondForm)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);
}

secondForm::~secondForm()
{
    delete ui;
}

void secondForm::mousePressEvent(QMouseEvent *event)
{
    m_Diff = event->pos();

    setCursor(QCursor(Qt::SizeAllCursor));
}

void secondForm::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);

    setCursor(QCursor(Qt::ArrowCursor));
}

void secondForm::mouseMoveEvent(QMouseEvent *event)
{
    QPoint p = event->globalPos();

    window()->move(p - m_Diff);
}


void secondForm::on_btn_historique_clicked()
{
    ui->btn_retrait_depot->setChecked(false);
    ui->stackedWidget_operation->setCurrentIndex(1);
    ui->tableView_historique->verticalHeader()->setVisible(false);
    ui->tableView_historique->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView_historique->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    /*si la connexion a reussi */
    if(Database::getConnexion()){
        QSqlQueryModel * modal = new QSqlQueryModel();
        QSqlQuery *query = new QSqlQuery();
        query->prepare("SELECT c.numero_compte as 'numero de compte', pers.nom , pers.prenom , typeo.nom as 'type' , o.montant , o.dateoperation as 'date' FROM operation o JOIN compte c ON o.idcompte = c.id JOIN type_operation typeo ON typeo.id = o.idtypeoperation JOIN personne pers ON pers.id = c.idpersonne");
        query->exec();

        modal->setQuery(*query);
        ui->tableView_historique->setModel(modal);
        /* sinon */
    }else{
        QMessageBox msgBox;
        msgBox.setText("Echec de connexion à la base de donnée.");
        msgBox.exec();
    }

}



void secondForm::on_btn_confirm_clicked()
{
    bool verification_1  = false;
    bool verification_2  = false;
    bool verification_3  = false;
    bool verification_4 = false;

    qint32 idcompte , etat_compte , solde_compte;
    QString numero_comptesaisie = ui->input_numerocompte_cible->text().trimmed();
    qint32 montant_saisie = ui->inputMontant->text().trimmed().toInt();
    qint32 type_operation;
    QString operation_choisie = ui->comboBox_type->currentText();
    QString Date_jour = QDate::currentDate().toString();

    /*Vérifier si le compte qui reçoit l'argent existe  */
    if(!numero_comptesaisie.isEmpty() || numero_comptesaisie.count() != 14){

        if(Database::getConnexion()){
            QSqlQuery query_1;
            query_1.exec("SELECT id , solde , etat FROM compte WHERE numero_compte = '"+numero_comptesaisie+"'");
            if(query_1.first()){
                idcompte = query_1.value(0).toInt();
                solde_compte = query_1.value(1).toInt();
                etat_compte = query_1.value(2).toInt();
                verification_1 = true;
            }else{
                QMessageBox msgBox;
                msgBox.setText("Aucun resultat ,  Ce compte n'existe pas.");
                msgBox.exec();
            }
        }

    }else{
        QMessageBox msgBox;
        msgBox.setText("Veuillez saisir le numero du compte du client . Ce numero comporte 14 chiffres .");
        msgBox.exec();
    }

    /*verification du montant lors du depot ou retrait */
    if(verification_1 && etat_compte == 1){
        if(operation_choisie == "DEPOT"){
            type_operation = 1;
            if(montant_saisie < 5000){
                QMessageBox msgBox;
                msgBox.setText("Le dépôt minimum doit être au moins égale à 5000 F CFCA.");
                msgBox.exec();
            }else{
                session_solde += montant_saisie;
                verification_2 = true;
            }
        }else{
            type_operation = 2;
            if(montant_saisie < 5000){
                QMessageBox msgBox;
                msgBox.setText("Le retrait minimum doit être au moins égale à 5000 F CFCA.");
                msgBox.exec();
            }else{
                session_solde -= montant_saisie;
                verification_2 = true;
            }
        }
    }else if(etat_compte == 0){
        QMessageBox msgBox;
        msgBox.setText("Le compte existe mais ne peut effectuer d'operation. RAISON : compte fermé .");
        msgBox.exec();
    }



    if(verification_2){
        /*si la connexion a reussi */
        QSqlQuery query_2;
        query_2.prepare("INSERT INTO operation(idcompte , idtypeoperation , montant , dateoperation) "
                        "VALUES(?,?,?,?)");
        query_2.addBindValue(idcompte);
        query_2.addBindValue(type_operation);
        query_2.addBindValue(montant_saisie);
        query_2.addBindValue(Date_jour);
        if(query_2.exec()){
            verification_3 = true;
        }else{
            QMessageBox msgBox;
            msgBox.setText("Une erreur est survenue pendant l'operation .");
            msgBox.exec();
        }
    }

    if(verification_3){
        QSqlQuery query_3;
        if(operation_choisie == "DEPOT"){
            query_3.exec("UPDATE compte SET solde = solde + "+QString::number(montant_saisie)+" WHERE numero_compte ='"+numero_comptesaisie+"'");
        }else{
            if(solde_compte >= montant_saisie){
                verification_4 = query_3.exec("UPDATE compte SET solde = solde - "+QString::number(montant_saisie)+" WHERE numero_compte ='"+numero_comptesaisie+"'");
                if(verification_4){
                    QMessageBox msgBox;
                    msgBox.setText("Operation effectué.");
                    msgBox.exec();
                    ui->inputMontant->clear();
                    ui->input_numerocompte_cible->clear();
                }else{
                    QMessageBox msgBox;
                    msgBox.setText("Erreur pendant l'operation , veuillez réessayer.");
                    msgBox.exec();
                }
            }else{
                QMessageBox msgBox;
                msgBox.setText("Operation impossible , le solde est insuffisant pour effectuer ce retrait.");
                msgBox.exec();
            }
        }
    }
}

void secondForm::on_btn_retrait_depot_clicked()
{
    ui->btn_historique->setChecked(false);
    ui->stackedWidget_operation->setCurrentIndex(0);
    /*si la connexion a reussi */
    if(Database::getConnexion()){
        QSqlQuery query;
        query.exec("SELECT numero , solde FROM compte WHERE idpersonne = "+QString::number(session_id));

        int compteur =0;
        while(query.next()){
            compteur++;
            session_compte =  query.value(0).toString();
            session_solde =  query.value(1).toInt();
        }
        /* sinon */
    }
}

void secondForm::on_btn_deconnexion_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("Êtes vous sure de vouloir quitter ?");
    msgBox.setInformativeText("vous serez totalement deconnecté et l'application se fermera automatiquement !");
    msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
    msgBox.setDefaultButton(QMessageBox::Yes);
    int ret = msgBox.exec();

    switch (ret) {
    case QMessageBox::Yes:
        qApp->quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
        break;
    case QMessageBox::No:
        // Don't Save was clicked
        break;
    default:
        // should never be reached
        break;
    }
}

void secondForm::on_btn_operation_clicked()
{
    ui->btn_client->setChecked(false);
    ui->btn_home->setChecked(false);
    ui->btn_param->setChecked(false);
    ui->stackedWidget->setCurrentIndex(0);
}

void secondForm::on_btn_client_clicked()
{
    ui->btn_operation->setChecked(false);
    ui->btn_param->setChecked(false);
    ui->btn_home->setChecked(false);
    ui->stackedWidget->setCurrentIndex(1);
}

void secondForm::on_btnClose_clicked()
{
    this->close();
}

void secondForm::on_btnMaxi_clicked()
{
    this->setWindowState(Qt::WindowMaximized);
}

void secondForm::on_btn_ouvrircompte_clicked()
{
    ui->btn_listcliente->setChecked(false);
    ui->btnRecherche->setChecked(false);
    ui->stackedWidget_client->setCurrentIndex(0);
}

void secondForm::on_btn_listcliente_clicked()
{
    ui->btn_ouvrircompte->setChecked(false);
    ui->btnRecherche->setChecked(false);
    ui->stackedWidget_client->setCurrentIndex(1);
    ui->tableView_client->verticalHeader()->setVisible(false);
    ui->tableView_client->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView_client->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    /*si la connexion a reussi */
    if(Database::getConnexion()){
        QSqlQueryModel * modal = new QSqlQueryModel();
        QSqlQuery *query = new QSqlQuery();
        query->prepare("SELECT numero_compte as 'numero de compte', iban  , nom , prenom , solde ,  contact , login as 'identifiant' , cni , CASE WHEN etat = 1 THEN 'activer' ELSE 'desactiver' END as status  FROM personne pers JOIN compte cpt ON pers.id = cpt.idpersonne WHERE idtypepersonne = 0");
        query->exec();

        modal->setQuery(*query);
        ui->tableView_client->setModel(modal);
        /* sinon */
    }else{
        QMessageBox msgBox;
        msgBox.setText("Echec de connexion à la base de donnée.");
        msgBox.exec();
    }
}

void secondForm::on_btnRecherche_clicked()
{
    ui->btn_ouvrircompte->setChecked(false);
    ui->btn_listcliente->setChecked(false);
    ui->stackedWidget_client->setCurrentIndex(2);
}

void secondForm::on_btn_annuler_2_clicked()
{
    ui->input_cni->clear();
    ui->input_numero->clear();
    ui->input_prenom->clear();
    ui->input_nom->clear();
}


void secondForm::on_btn_creercompte_clicked()
{
    QString nom = ui->input_nom->text().trimmed();
    QString prenom = ui->input_prenom->text().trimmed();
    QString identifiant = Generator::identifiant(nom);
    QString numero_compte =  Generator::compteBancaire();
    QString numero_iban = QString::number(225)+numero_compte;
    QString contact = ui->input_numero->text().trimmed();
    QString cni = ui->input_cni->text().trimmed();
    QString sexe;

    if(ui->radioButton_sexeM->isChecked()){
        sexe = "m";
    }

    if(ui->radioButton_sexeF->isChecked()){
        sexe = "f";
    }

    qint32 lastid ;

    bool valider_1 = false;
    bool valider_2 = false;
    bool valider_3 = false;
    bool valider_4 = false;
    bool valider_5 = false;

    /* verification des champs */
    if(nom.isEmpty() || prenom.isEmpty() || contact.isEmpty() || cni.isEmpty()){
        QMessageBox msgBox;
        msgBox.setText("Veuillez remplir tous les champs .");
        msgBox.exec();
    }else if(nom.count() < 3 || prenom.count()< 3){
        QMessageBox msgBox;
        msgBox.setText("Le champ Nom ou Prénom est réquis et comporte au moins 3 caractères .");
        msgBox.exec();
    }else if(contact.count() != 8){
        QMessageBox msgBox;
        msgBox.setText("Le champ Numero doit comporter 8 caractères .");
        msgBox.exec();
    }else{
        valider_1 = true;
    }

    /*verification connexion base de donnée */
    if(valider_1){
        if(Database::getConnexion()){
            valider_2 = true;
        }else{
            QMessageBox msgBox;
            msgBox.setText("Erreur de connexion à la base de donnée.");
            msgBox.exec();
        }
    }

    /* verification nom et prénom */
    if(valider_2){
        QSqlQuery query_1;
        /* verifier si la personne est dejà inscrite  */
        query_1.exec("SELECT * FROM personne WHERE nom ='"+nom+"' and prenom = '"+prenom+"'");
        if(query_1.first()){
            QMessageBox msgBox;
            msgBox.setText("Une personne possédant le même Nom et Prénom est déjà enregistré .");
            msgBox.exec();
        }else{
            valider_3 = true;
        }
    }


    /*enregistrement de la personne */
    if(valider_3){
        QSqlQuery query_2;
        query_2.prepare("INSERT INTO personne(login ,motdepasse, nom , prenom, contact , cni , sexe) VALUES(?,?,?,?,?,?,?)");
        query_2.addBindValue(identifiant);
        query_2.addBindValue("ddd");
        query_2.addBindValue(nom);
        query_2.addBindValue(prenom);
        query_2.addBindValue(contact);
        query_2.addBindValue(cni);
        query_2.addBindValue(sexe);
        if(query_2.exec()){
            lastid = query_2.lastInsertId().toInt();
            valider_4 = true;
        }else{
            QMessageBox msgBox;
            qDebug() << "Error:" << query_2.lastError();
            msgBox.setText("Un problème est survenue pendant la création du compte.");
            msgBox.exec();
        }
    }


    /*Enregistrement  du compte bancaire*/
    if(valider_4){
        QSqlQuery query_3;
        query_3.prepare("INSERT INTO compte(idpersonne ,numero_compte, iban , dateouverture ) VALUES(?,?,?,?)");
        query_3.addBindValue(lastid);
        query_3.addBindValue(numero_compte);
        query_3.addBindValue(numero_iban);
        QString Date_jour = QDate::currentDate().toString();
        query_3.addBindValue(Date_jour);
        if(query_3.exec()){
            valider_5 = true;
        }else{
            QMessageBox msgBox;
            qDebug() << "Error:" << query_3.lastError();
            msgBox.setText("Un problème est survenue pendant la création du compte.");
            msgBox.exec();
        }
    }

    if(valider_5){
        QMessageBox msgBox;
        msgBox.setText("Le compte vient d'être crée avec succès .");
        msgBox.exec();
        secondForm::on_btn_annuler_2_clicked();
    }

}

void secondForm::on_btnRecherche_2_clicked()
{
    QString recherche = ui->input_recherche->text();
    if(recherche.isEmpty()){
        QMessageBox msgBox;
        msgBox.setText("Le champ recherche ne peut être vide");
        msgBox.exec();
    }

    QSqlQuery query_1;
    /* verifier si la personne est dejà inscrite  */
    query_1.exec("SELECT p.nom , p.prenom , p.contact , cmp.numero_compte ,cmp.solde , cmp.iban , cmp.etat FROM personne p JOIN compte cmp ON p.id = cmp.idpersonne WHERE  p.cni ='"+recherche+"'");
    if(query_1.first()){
        ui->label_nom->setText(query_1.value(0).toString());
        ui->label_prenom->setText(query_1.value(1).toString());
        ui->label_contact->setText(query_1.value(2).toString());
        ui->label_numerocompte->setText(query_1.value(3).toString());
        ui->label_solde->setText(query_1.value(4).toString()+"F CFA");
        ui->label_iban->setText(query_1.value(5).toString());
        qint32 etat = query_1.value(6).toInt();
        if(etat == 1){
            ui->label_statuscompte->setText("Activé");
        }else{
            ui->label_statuscompte->setText("Desactivé");
        }
    }else{
        QMessageBox msgBox;
        msgBox.setText("Aucun resultat.");
        msgBox.exec();
    }

}

void secondForm::on_btn_efccetuervirement_clicked()
{

}

void secondForm::on_btnMini_clicked()
{
    this->showMinimized();
}


void secondForm::on_btn_Menu_clicked()
{
    if(ui->sideBar->maximumWidth() == 200 ){
        ui->sideBar->setMaximumWidth(0);
    }else{
        ui->sideBar->setMaximumWidth(200);
    }
}

void secondForm::on_btn_changemdp_clicked()
{
    QString nouveau = ui->lineEdit_nouveaumdp->text();
    QString ancien = ui->lineEdit_ancienmpd->text();

    if(ancien.isEmpty() || nouveau.isEmpty()){
        QMessageBox msgBox;
        msgBox.setText("Remplissez tous les champs SVP");
        msgBox.exec();
    }else if(ancien != session_pass){
        QMessageBox msgBox;
        msgBox.setText("Votre ancien mot de passe est incorrecte.");
        msgBox.exec();
    }else if(nouveau.count() < 6){
        QMessageBox msgBox;
        msgBox.setText("Le nouveau mot de passe requiert au moins 6 caracères alphanumeriques .");
        msgBox.exec();
    }else{
        QSqlQuery query;
        bool reponse = query.exec("UPDATE personne SET motdepasse = "+nouveau+" WHERE id ="+QString::number(session_id));
        if(reponse){
            QMessageBox msgBox;
            msgBox.setText("mot de passe mis à jour . Merci");
            msgBox.exec();
        }else{
            QMessageBox msgBox;
            msgBox.setText("Une erreur est survenue , veuillez reprendre SVP.");
            msgBox.exec();
        }
    }
}

void secondForm::on_btn_param_clicked()
{
    ui->btn_operation->setChecked(false);
    ui->btn_client->setChecked(false);
    ui->btn_home->setChecked(false);
    ui->btn_home->setChecked(false);
    ui->stackedWidget->setCurrentIndex(2);
}

void secondForm::on_btn_home_clicked()
{
    ui->btn_param->setChecked(false);
    ui->btn_operation->setChecked(false);
    ui->btn_client->setChecked(false);
    ui->btn_home->setChecked(true);
    ui->stackedWidget->setCurrentIndex(3);

    QSqlQuery query;
    /* 1- */
    query.exec("SELECT count(*) FROM compte");
    query.first();
    QString val1 = query.value(0).toString();
    ui->label_client->setText(val1);
    query.finish();
    /* 2- */
    query.exec("SELECT count(*) FROM operation");
    query.first();
    QString val2 = query.value(0).toString();
    ui->label_operation->setText(val2);
    query.finish();
}

void secondForm::showEvent(QShowEvent *e)
{
     ui->label_nom_prenom->setText(session_nom);
     QSqlQuery query;
     /* 1- */
     query.exec("SELECT count(*) FROM compte");
     query.first();
     QString val1 = query.value(0).toString();
     ui->label_client->setText(val1);
     query.finish();
     /* 2- */
     query.exec("SELECT count(*) FROM operation");
     query.first();
     QString val2 = query.value(0).toString();
     ui->label_operation->setText(val2);
     query.finish();

     secondForm::on_btn_listcliente_clicked();
}

void secondForm::on_tableView_client_clicked(const QModelIndex &index)
{

}

void secondForm::on_btn_desactiver_clicked()
{
    QModelIndex index= ui->tableView_client->selectionModel()->currentIndex();
    QString value= index.sibling(index.row(),0).data().toString();
    QString etat = index.sibling(index.row(),8).data().toString();

    if(etat  == "desactiver"){
        QMessageBox msgBox;
        msgBox.setText("Le compte est déjà desactivé .");
        msgBox.exec();
    }else{

        if(!value.isNull()){
            QSqlQuery query;
            bool reponse = query.exec("UPDATE compte SET etat = 0 WHERE numero_compte = '"+value+"'");
            if(reponse){
                QMessageBox msgBox;
                msgBox.setText("Le compte vient d'être verouillé .");
                msgBox.exec();
                secondForm::reactualiser_client();
            }else{
                QMessageBox msgBox;
                msgBox.setText("Une erreur est survenue , veuillez reprendre SVP.");
                msgBox.exec();
            }
        }else{
            QMessageBox msgBox;
            msgBox.setText("Selectionner une ligne.");
            msgBox.exec();
        }
    }

}

void secondForm::on_btn_reouvrircpt_clicked()
{
    QModelIndex index= ui->tableView_client->selectionModel()->currentIndex();
    QString value= index.sibling(index.row(),0).data().toString();
    QString etat = index.sibling(index.row(),8).data().toString();

    if(etat  == "activer"){
        QMessageBox msgBox;
        msgBox.setText("Le compte est déjà activé .");
        msgBox.exec();
    }else{

        if(!etat.isEmpty()){
            QSqlQuery query;
            bool reponse = query.exec("UPDATE compte SET etat = 1 WHERE numero_compte = '"+value+"'");
            if(reponse){
                QMessageBox msgBox;
                msgBox.setText("Le compte vient d'être reouvert .");
                msgBox.exec();
                secondForm::reactualiser_client();
            }else{
                QMessageBox msgBox;
                msgBox.setText("Une erreur est survenue , veuillez reprendre SVP.");
                msgBox.exec();
            }
        }else{
            QMessageBox msgBox;
            msgBox.setText("selectionner une ligne avant de reactiver un compte.");
            msgBox.exec();
        }

    }

}

void secondForm::on_tableView_client_activated(const QModelIndex &index)
{
}

void secondForm::reactualiser_client(){
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery *query = new QSqlQuery();
    query->prepare("SELECT numero_compte as 'numero de compte', iban  , nom , prenom , solde ,  contact , login as 'identifiant' , cni , CASE WHEN etat = 1 THEN 'activer' ELSE 'desactiver' END as status  FROM personne pers JOIN compte cpt ON pers.id = cpt.idpersonne WHERE idtypepersonne = 0");
    query->exec();
    modal->setQuery(*query);
    ui->tableView_client->setModel(modal);
}
