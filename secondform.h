#ifndef SECONDFORM_H
#define SECONDFORM_H

#include <QMainWindow>

namespace Ui {
class secondForm;
}

class secondForm : public QMainWindow
{
    Q_OBJECT

public:
    explicit secondForm(QWidget *parent = 0);
    ~secondForm();
    void mousePressEvent  (QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent   (QMouseEvent *event);

private slots:

    void showEvent(QShowEvent *e);
    void on_btn_historique_clicked();

    void on_btn_confirm_clicked();

    void on_btn_retrait_depot_clicked();

    void on_btn_deconnexion_clicked();

    void on_btn_operation_clicked();

    void on_btn_client_clicked();

    void on_btnClose_clicked();

    void on_btnMaxi_clicked();

    void on_btn_ouvrircompte_clicked();

    void on_btn_listcliente_clicked();

    void on_btnRecherche_clicked();

    void on_btn_annuler_2_clicked();

    void on_btn_creercompte_clicked();

    void on_btnRecherche_2_clicked();

    void on_btn_efccetuervirement_clicked();

    void on_btnMini_clicked();

    void on_btnvirement_clicked();

    void on_btn_Menu_clicked();

    void on_btn_changemdp_clicked();

    void on_btn_param_clicked();

    void on_btn_home_clicked();

    void on_tableView_client_clicked(const QModelIndex &index);

    void on_btn_desactiver_clicked();

    void on_btn_reouvrircpt_clicked();

    void on_tableView_client_activated(const QModelIndex &index);
    void reactualiser_client();

private:
    Ui::secondForm *ui;
    QPoint m_Diff;
    QString line_selected;
};

#endif // SECONDFORM_H
